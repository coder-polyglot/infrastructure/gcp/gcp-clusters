resource "google_compute_address" "ingress_ip" {
  name         = "${var.infra_layer}-cluster-ip"
  address_type = "EXTERNAL"
}
