provider "cloudflare" {
  email      = var.cloudflare_email
  api_key    = var.cloudflare_api_key
  account_id = var.cloudflare_account_id
}

# Create a record# Add a record to the domain
resource "cloudflare_record" "coderpolyglot_a_record" {
  zone_id = var.cloudflare_zone_id
  name    = var.infra_layer == "frontend" ? var.cloudflare_front_record_name : var.cloudflare_back_record_name
  value   = google_compute_address.ingress_ip.address
  type    = "A"
  ttl     = 1
  proxied = true

  depends_on = [google_compute_address.ingress_ip]
}
