# ---------------------------------------------------------------------------------------------------------------------
# DEPLOY A SAMPLE CHART
# A chart repository is a location where packaged charts can be stored and shared. Define Bitnami Helm repository location,
# so Helm can install the nginx chart.
# ---------------------------------------------------------------------------------------------------------------------

resource "helm_release" "ingress" {
  depends_on = [google_container_node_pool.node_pool, google_compute_address.ingress_ip]

  repository = "https://charts.bitnami.com/bitnami"
  name       = "nginx-ingress-controller"
  chart      = "nginx-ingress-controller"

  set {
    name  = "service.loadBalancerIP"
    value = google_compute_address.ingress_ip.address
  }
}
