project                      = "cp-backend-prod"
location                     = "us-central1"
region                       = "us-central1"
cluster_name                 = "cp-backend-cluster"
cluster_service_account_name = "coderpolyglot-backend-prod"
infra_layer                  = "backend"

############# CLOUDFLARE VARIABLES ####################
cloudflare_record_domain     = "coderpolyglot.com"
cloudflare_record_name       = "coderpolyglot.com"
cloudflare_front_record_name = "coderpolyglot.com"
cloudflare_back_record_name  = "api"
