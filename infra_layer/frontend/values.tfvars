project                      = "cp-frontend-prod"
location                     = "us-central1"
region                       = "us-central1"
cluster_name                 = "cp-frontend-cluster"
cluster_service_account_name = "coderpolyglot-frontend-prod"
infra_layer                  = "frontend"

############# CLOUDFLARE VARIABLES ####################
cloudflare_record_domain     = "coderpolyglot.com"
cloudflare_record_name       = "coderpolyglot.com"
cloudflare_front_record_name = "coderpolyglot.com"
cloudflare_back_record_name  = "api"
